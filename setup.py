import versioneer
from setuptools import setup, find_packages


with open('requirements.txt') as f:
    REQUIREMENTS = f.read().splitlines()

with open('README.md') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='drb-topic-editor',
    packages=find_packages(),
    description='DRB Topic Editor',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='GAEL Systems',
    author_email='drb-python@gael.fr',
    url='https://gitlab.com/drb-python/tools/topic-editor',
    python_requires='>=3.8',
    install_requires=REQUIREMENTS,
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI :: Apache Software License",
        "Operating System :: Unix",
        "Operating System :: MacOS"
    ],
    project_urls={
        'Source': 'https://gitlab.com/drb-python/tools/topic-editor',
    },
    data_files=[('.', ['requirements.txt'])],
    entry_points={
        'console_scripts': [
            'drb-topic-editor=drb_topic_editor:main'
        ]
    }
)
