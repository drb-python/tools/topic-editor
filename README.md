# DRB Topic Editor
The **DRB Topic Editor** is a graphical software helping to create and edit
item classes.

## Functionalities
 - display the hierarchic tree of item classes defined in the current
**Python** environment
 - display the hierarchic tree of item classes which are in _work in progress_
 - display / edit  definition of an item class
 - save/load the _work in progress_ tree from/to a file
 - generate the relational graph of an item class

## Installation
#### Debian / Ubuntu
    sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0 graphviz libgirepository1.0-dev
    pip install -r drb-topic-editor

#### MacOS
    brew install pygobject3 gtk3 graphviz
    pip install drb-topic-editor

#### Windows
1. Go to http://www.msys2.org/ and download the x86_64 installer
2. Follow the instructions on the page for setting up the basic environment
3. Run `C:\path\to\mingw64.exe` - a terminal window should pop up
4. Execute `pacman -Suy`
5. Execute `pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-python3 mingw-w64-x86_64-python3-gobject mingw-w64-x86_64-python3-gobject`
5. Execute `pacman -S mingw-w64-python-pip mingw-w64-graphviz mingw-w64-python-graphviz`
6. Execute `pip install drb-topic-editor`

## User Guide
### Views
Two views are defined in the `DRB Topic Editor`:
 - _work in progress_ (default)
 - _Python environment_

To switch view click on the `View > Environment` button from the menu, to
activate or deactivate the _Python environment_ view.

#### Work in progress view
This view allowing to Create/Read/Update/Delete (**CRUD**) operations on item
classes defined in this view.

#### Python environment view
Like the _work in progress_ view the _Python environment_ view allowing to
browse (on read-only) item classes defined in the **Python** environment where
`DRB Topic Editor` is running.

## Persistence
### Saving
Save functionality allowing to write in a file all item classes defined in the
_work in progress_ view. To save click on the `File > Save` button from the
menu. 
### Loading
Load functionality allowing to resume a work from a file containing item
classes. Caution it will erase all content in the _work in progress_ view.
To load click on the `File > Load` button from the menu.