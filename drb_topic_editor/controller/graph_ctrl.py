from gi.repository import Gtk, GdkPixbuf
from ..model import ModelManager
from ..view import GraphWindow, FileChooser
from ..graph import GraphManager


class GraphController:
    ENGINES = (
        'dot',
        'neato',
        'twopi',
        'circo',
        'fdp',
        'osage',
        'patchwork',
        'sfdp',
    )

    ORIENTATIONS = ('TB', 'BT', 'LR', 'RL')

    def __init__(self, model: ModelManager):
        self.model = model
        self.view = GraphWindow()
        self.graph_manager = GraphManager(model)

    def initialize(self):
        app = self.view.get_application()
        # window
        self.view.set_default_size(app.get_default_width(),
                                   app.get_default_height())
        self.view.connect('destroy', self.close)
        # menu
        self.view.menu.file_save.connect('activate',
                                         self.on_activated_file_save)
        # engine
        for engine in self.ENGINES:
            self.view.params.engine.combobox.append_text(engine)
        self.view.params.engine.combobox.set_active(0)
        self.view.params.engine.combobox.connect('changed',
                                                 self.do_update_params)
        # orientation
        for orientation in self.ORIENTATIONS:
            self.view.params.orientation.combobox.append_text(orientation)
        self.view.params.orientation.combobox.set_active(0)
        self.view.params.orientation.combobox.connect('changed',
                                                      self.do_update_params)
        # size
        adjustment = Gtk.Adjustment(lower=3,
                                    upper=12,
                                    step_increment=0.1,
                                    value=8)
        self.view.params.size.button.set_adjustment(adjustment)
        self.view.params.size.button.set_digits(1)
        self.view.params.size.button.connect('value-changed',
                                             self.do_update_params)
        self.view.params.size.button.connect('change-value',
                                             self.do_update_params)
        # graph
        path = self.graph_manager.generate()
        self.display_image_file(path)

    def display_image_file(self, image_file: str):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(image_file)
        self.view.graph.set_from_pixbuf(pixbuf)
        self.view.graph.show_all()

    def on_activated_file_save(self, activated):
        path = None
        file_chooser = FileChooser(Gtk.FileChooserAction.SAVE)
        file_chooser.set_do_overwrite_confirmation(True)
        response = file_chooser.run()
        if response == Gtk.ResponseType.OK:
            path = file_chooser.get_filename()
        file_chooser.destroy()
        if path is not None:
            self.graph_manager.save_graph_to(path)

    def do_update_params(self, changed):
        params = self.view.params
        path = self.graph_manager.generate(
            engine=params.engine.combobox.get_active_text(),
            orientation=params.orientation.combobox.get_active_text(),
            size=params.size.button.get_value())
        self.display_image_file(path)

    def close(self, widget):
        self.graph_manager.close()
