from gi.repository import Gtk
from typing import List
from drb.factory import ItemClassType
from ..model import ModelManager, ItemClassModel, SignatureModel, \
    AttributeModel, ChildModel
from ..view import FileChooser, ItemClassWindow, SignaturePage, \
    AttributePage, ChildPage


def retrieve_highest_itemclasses(item_classes: List[ItemClassModel]):
    ids = [x.id for x in item_classes]
    return [x for x in item_classes
            if x.subclass_of is None or x.subclass_of not in ids]


class ItemClassController:
    def __init__(self, model: ModelManager):
        self.model = model
        self.view = ItemClassWindow()
        self.tree_selection_callback_id = None

    def _itemclasses_to_treestore(self, item_classes) -> Gtk.TreeStore:
        store = Gtk.TreeStore(str, str)
        highest = retrieve_highest_itemclasses(item_classes)
        for ic in highest:
            self._internal_itemclasses_to_treestore(
                store, item_classes, ic, None)
        return store

    def _internal_itemclasses_to_treestore(self, store: Gtk.TreeStore,
                                           item_classes, ic: ItemClassModel,
                                           pnode: Gtk.TreeIter = None):
        children_cls = [x for x in item_classes if x.subclass_of == ic.id]
        parent = store.append(pnode, [ic.label, ic.id])
        for child_cls in children_cls:
            self._internal_itemclasses_to_treestore(store, item_classes,
                                                    child_cls, parent)

    def initialize(self):
        app = self.view.get_application()
        # prepare window
        self.view.set_default_size(app.get_default_width(),
                                   app.get_default_height())
        self.view.connect('destroy', self.destroy)

        # menu
        menu = self.view.menu
        menu.file_new.connect('activate', self.on_activate_menu_file_new)
        menu.file_load.connect('activate', self.on_activate_menu_file_load)
        menu.file_save.connect('activate', self.on_activate_menu_file_save)
        menu.file_exit.connect('activate', self.destroy)
        menu.view_env.set_active(False)
        menu.view_env.connect('toggled', self.on_toggle_view_menu)
        menu.view_graph.connect('activate', self.on_activate_menu_graph)

        # tree
        item_classes = self.model.working
        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn('Item Classes', renderer, text=0)
        tree = self.view.tree
        tree.set_model(self._itemclasses_to_treestore(item_classes))
        tree.append_column(column)
        self.tree_selection_callback_id = tree.get_selection().connect(
            'changed', self.on_select_item_class, True)

        # info category model
        for category in self.model.get_itemclass_types():
            self.view.info.category.combobox.append_text(category.name)
        self.view.info.category.combobox.set_active(
            self.model.get_itemclass_type_index(ItemClassType.CONTAINER))

        # info factory model
        # TODO loading factories

        # info
        self.set_ready_only_mode(True)
        info = self.view.info
        info.id.entry.set_editable(False)
        info.label.entry.connect('changed', self.on_changed_ic_label)
        info.subclass_of.combobox.connect(
            'changed', self.on_changed_ic_subclass)
        info.category.combobox.connect('changed', self.on_changed_ic_category)
        info.description.text.get_buffer().connect(
            'changed', self.on_changed_ic_description)
        info.factory.combobox.connect('changed', self.on_changed_ic_factory)
        info.signatures.buttons.add_button.connect(
            'clicked', self.on_added_ic_signature)
        info.signatures.buttons.del_button.connect(
            'clicked', self.on_removed_ic_signature)

    def destroy(self, *args, **kwargs):
        self.view.get_application().remove_window(self.view)

    def on_activate_menu_file_new(self, activated):
        # generating new editable item class
        icm = ItemClassModel()
        self.model.working.append(icm)
        self.model.current = icm

        # add the newest item class in working tree
        store = self.view.tree.get_model()
        tree_itr = store.append(None, [icm.label, icm.id])
        # force selection of the newest item class
        selection = self.view.tree.get_selection()
        selection.select_iter(tree_itr)

    def on_activate_menu_file_load(self, activated):
        path = None
        file_chooser = FileChooser(Gtk.FileChooserAction.OPEN)
        response = file_chooser.run()
        if response == Gtk.ResponseType.OK:
            path = file_chooser.get_filename()
        file_chooser.destroy()
        if path is not None:
            self.model.load(path)
            self.update_tree_store()
            tree = self.view.tree
            tree.get_selection().select_iter(tree.get_model().get_iter_first())

    def on_activate_menu_file_save(self, activated):
        path = None
        file_chooser = FileChooser(Gtk.FileChooserAction.SAVE)
        file_chooser.set_do_overwrite_confirmation(True)
        response = file_chooser.run()
        if response == Gtk.ResponseType.OK:
            path = file_chooser.get_filename()
        file_chooser.destroy()
        if path is not None:
            with open(path, 'w') as file:
                self.model.dump(file)

    def on_activate_menu_graph(self, activated):
        app = self.view.get_application()
        app.switch_to_graph_view()
        self.model.current = None

    def on_toggle_view_menu(self, toggled: Gtk.CheckMenuItem):
        if toggled.get_active():
            self.commit_change()
        self.update_tree_store()

    def set_ready_only_mode(self, mode: bool):
        active = not mode
        if self.model.current is None or self.view.menu.view_env.get_active():
            active = False

        info = self.view.info
        info.label.entry.set_sensitive(active)
        info.subclass_of.combobox.set_sensitive(active)
        info.category.combobox.set_sensitive(active)
        info.description.text.set_sensitive(active)
        info.factory.combobox.set_sensitive(active)
        widget = info.signatures
        widget.buttons.add_button.set_sensitive(active)
        widget.buttons.del_button.set_sensitive(active)
        for i in range(widget.notebook.get_n_pages()):
            page: SignaturePage = widget.notebook.get_nth_page(i)
            page.set_sensitive(active)

    def update_tree_store(self, changed=None):
        item_classes = self.model.working
        ready_only = self.view.menu.view_env.get_active()
        if ready_only:
            item_classes = self.model.loaded
        store = self._itemclasses_to_treestore(item_classes)
        self.view.tree.set_model(store)
        self.view.tree.get_selection().unselect_all()
        self.set_ready_only_mode(not ready_only)

    def on_select_item_class(self, selector: Gtk.TreeSelection,
                             recursive=True):
        icm = self.model.current
        if icm in self.model.working:
            self.commit_change()

        store, itr = selector.get_selected()
        if itr is None:
            self.model.current = None
        else:
            self.model.current = self.model.get_itemclass(store[itr][1])
        if recursive:
            self.refresh_item_class_info()
        self.set_ready_only_mode(self.view.menu.view_env.get_active())

    def _load_attribute_from_page(self, page: AttributePage) -> AttributeModel:
        return AttributeModel(page.name.entry.get_text(),
                              page.namespace.entry.get_text(),
                              page.value.entry.get_text())

    def _load_child_from_page(self, page: ChildPage) -> ChildModel:
        return ChildModel(page.name.entry.get_text(),
                          page.namespace.entry.get_text(),
                          page.ns_aware.get_active())

    def _load_signature_from_page(self, page: SignaturePage) -> SignatureModel:
        sig = SignatureModel()
        if page.button_name.get_active():
            sig.name = page.name.entry.get_text()
        if page.button_namespace.get_active():
            sig.namespace = page.namespace.entry.get_text()
        if page.button_path.get_active():
            sig.path = page.path.entry.get_text()
        if page.button_attributes.get_active():
            for i in range(page.attributes.notebook.get_n_pages()):
                p: AttributePage = page.attributes.notebook.get_nth_page(i)
                attr = AttributeModel(p.name.entry.get_text(),
                                      p.namespace.entry.get_text(),
                                      p.value.entry.get_text())
                if len(sig.attributes) == i:
                    sig.attributes.append(attr)
                else:
                    sig.attributes[i] = attr
        if page.button_children.get_active():
            for i in range(page.children.notebook.get_n_pages()):
                p: ChildPage = page.attributes.notebook.get_nth_page(i)
                child = ChildModel(p.name.entry.get_text(),
                                   p.namespace.entry.get_text(),
                                   p.ns_aware.get_active())
                if len(sig.children) == i:
                    sig.children.append(child)
                else:
                    sig.children[i] = child
        if page.button_xquery.get_active():
            buffer = page.xquery.text.get_buffer()
            sig.xquery = buffer.get_text(
                buffer.get_start_iter(), buffer.get_end_iter(), False)
        if page.button_python.get_active():
            buffer = page.python.text.get_buffer()
            sig.python = buffer.get_text(
                buffer.get_start_iter(), buffer.get_end_iter(), False)
        return sig

    def commit_change(self):
        icm = self.model.current
        if icm is None:
            return

        info = self.view.info
        icm.label = info.label.entry.get_text()
        icm.subclass_of = None
        subclass_id = info.subclass_of.combobox.get_active()
        subclass = self.model.current_possible_subclasses[subclass_id]
        if subclass_id > -1 and subclass is not None:
            icm.subclass_of = subclass.id
        icm.category = self.model.get_itemclass_type_index(
            ItemClassType(info.category.combobox.get_active_text()))
        buffer = info.description.text.get_buffer()
        icm.description = buffer.get_text(buffer.get_start_iter(),
                                          buffer.get_end_iter(),
                                          False)
        for i in range(info.signatures.notebook.get_n_pages()):
            page = info.signatures.notebook.get_nth_page(i)
            sig = self._load_signature_from_page(page)
            if len(icm.signatures) == i:
                icm.signatures.append(sig)
            else:
                icm.signatures[i] = sig

    def refresh_item_class_info(self):
        icm = self.model.current
        info = self.view.info
        if icm is None:
            info.id.entry.set_text('')
            info.label.entry.set_text('')
            info.subclass_of.combobox.remove_all()
            info.subclass_of.combobox.append_text('-')
            info.subclass_of.combobox.set_active(0)
            info.category.combobox.set_active(
                self.model.get_itemclass_type_index(ItemClassType.CONTAINER))
            info.description.text.get_buffer().set_text('')
            info.factory.combobox.remove_all()
            info.factory.combobox.append_text('-')
            info.factory.combobox.set_active(0)
            info.signatures.clear()
        else:
            info.id.entry.set_text(icm.id)
            info.label.entry.set_text(icm.label)
            info.category.combobox.set_active(icm.category)
            info.description.text.get_buffer().set_text(icm.description)
            # subclass of
            info.subclass_of.combobox.set_active(-1)
            info.subclass_of.combobox.remove_all()
            for item in self.model.current_possible_subclasses:
                if item is None:
                    info.subclass_of.combobox.append_text('-')
                else:
                    info.subclass_of.combobox.append_text(item.label)
            subclass = self.model.current.subclass_of
            idx = self.model.current_possible_subclasses.index(
                self.model.get_itemclass(subclass)) \
                if subclass is not None else 0
            info.subclass_of.combobox.set_active(idx)
            # factory
            info.factory.combobox.remove_all()
            info.factory.combobox.append_text('-')
            info.factory.combobox.set_active(0)
            # signatures
            self._refresh_item_class_signatures()

    def _refresh_item_class_signatures(self):
        info = self.view.info
        icm = self.model.current
        info.signatures.clear()
        for sig in icm.signatures:
            sig_page = SignaturePage()
            info.signatures.append_page(sig_page)
            sig_page.initialize()
            # signature name section
            if sig.name is None:
                sig_page.button_name.set_active(False)
            else:
                sig_page.button_name.set_active(True)
                sig_page.name.entry.set_text(sig.name)
            # signature namespace section
            if sig.namespace is None:
                sig_page.button_namespace.set_active(False)
            else:
                sig_page.button_namespace.set_active(True)
                sig_page.namespace.entry.set_text(sig.namespace)
            # signature path section
            if sig.path is None:
                sig_page.button_path.set_active(False)
            else:
                sig_page.button_path.set_active(True)
                sig_page.path.entry.set_text(sig.path)
            # signature attributes section
            button = sig_page.attributes.buttons.add_button
            button.connect('clicked', self.on_added_attribute)
            button = sig_page.attributes.buttons.del_button
            button.connect('clicked', self.on_removed_attribute)
            if len(sig.attributes) > 0:
                sig_page.button_attributes.set_active(True)
                for attr in sig.attributes:
                    attr_page: AttributePage = AttributePage()
                    attr_page.name.entry.set_text(attr.name)
                    attr_page.namespace.entry.set_text(attr.namespace)
                    attr_page.value.entry.set_text(attr.value)
                    sig_page.attributes.append_page(attr_page)
            else:
                sig_page.button_attributes.set_active(False)
            # signature children section
            button = sig_page.children.buttons.add_button
            button.connect('clicked', self.on_added_child)
            button = sig_page.children.buttons.del_button
            button.connect('clicked', self.on_removed_child)
            if len(sig.children) > 0:
                sig_page.button_children.set_active(True)
                for child in sig.children:
                    child_page: ChildPage = ChildPage()
                    child_page.name.entry.set_text(child.name)
                    child_page.namespace.entry.set_text(child.namespace)
                    child_page.ns_aware.set_active(child.namespace_aware)
                    sig_page.children.append_page(child_page)
            else:
                sig_page.button_children.set_active(False)
            # signature xquery section
            if sig.xquery is None:
                sig_page.button_xquery.set_active(False)
            else:
                sig_page.button_xquery.set_active(True)
                sig_page.xquery.text.get_buffer().set_text(sig.xquery)
            # signature python section
            if sig.python is None:
                sig_page.button_python.set_active(False)
            else:
                sig_page.button_python.set_active(True)
                sig_page.python.text.get_buffer().set_text(sig.python)

    def on_changed_ic_label(self, entry: Gtk.Entry):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        icm = self.model.current
        icm.label = entry.get_text()
        store, itr = self.view.tree.get_selection().get_selected()
        if itr is not None:
            store.set_value(itr, 0, entry.get_text())

    def on_changed_ic_subclass(self, combobox: Gtk.ComboBoxText, force=False):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return

        if not force:
            return

        selection = self.view.tree.get_selection()
        store, itr = selection.get_selected()
        if itr is None:
            return

        icm_id = store[itr][1]
        self.model.current.subclass_of = None
        subclass = self.model.current_possible_subclasses[
            combobox.get_active()]
        if combobox.get_active() > -1 and subclass is not None:
            self.model.current.subclass_of = subclass.id
        self.update_tree_store()

        store = self.view.tree.get_model()
        itr = store.get_iter_first()
        while store[itr][1] != icm_id:
            itr = store.iter_next(itr)
        selection.emit('changed')
        # selection.select_iter(itr)

    def on_changed_ic_category(self, combobox: Gtk.ComboBoxText):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        self.model.current.category = self.model.get_itemclass_type_index(
            ItemClassType(self.view.info.category.combobox.get_active_text()))

    def on_changed_ic_description(self, entry: Gtk.Entry):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        buffer = self.view.info.description.text.get_buffer()
        self.model.current.description = buffer.get_text(
            buffer.get_start_iter(), buffer.get_end_iter(), False)

    def on_changed_ic_factory(self, entry: Gtk.Entry):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        pass

    def on_added_ic_signature(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        signatures = self.view.info.signatures
        # save possible update
        for i in range(signatures.notebook.get_n_pages()):
            signature = self._load_signature_from_page(
                signatures.notebook.get_nth_page(i))
            self.model.current.signatures[i] = signature
        # add new signature to the current item class model
        idx = len(self.model.current.signatures)
        self.model.current.signatures.append(SignatureModel())
        # refresh signatures section
        self._refresh_item_class_signatures()
        signatures.notebook.set_current_page(idx)

    def on_removed_ic_signature(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        current = self.model.current
        notebook = self.view.info.signatures.notebook

        idx = notebook.get_current_page()
        if idx > -1:
            # save possible update
            for i in range(notebook.get_n_pages()):
                if i != idx:
                    signature = self._load_signature_from_page(
                        notebook.get_nth_page(i))
                    self.model.current.signatures[i] = signature
            # remove signature and corresponding page
            widget = notebook.get_nth_page(idx)
            notebook.remove_page(idx)
            widget.destroy()
            del current.signatures[idx]
            # refresh signatures section
            self._refresh_item_class_signatures()

    def on_added_attribute(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        notebook = self.view.info.signatures.notebook
        sig_idx = notebook.get_current_page()
        sig: SignaturePage = notebook.get_nth_page(sig_idx)
        notebook = sig.attributes.notebook

        # save others attributes before to add a new one
        model_sig: SignatureModel = self.model.current.signatures[sig_idx]
        for i in range(notebook.get_n_pages()):
            page = notebook.get_nth_page(i)
            model_sig.attributes[i] = self._load_attribute_from_page(page)

        model_sig.attributes.append(AttributeModel(''))
        self._refresh_item_class_signatures()
        notebook.set_current_page(len(model_sig.attributes))

    def on_removed_attribute(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return

        notebook = self.view.info.signatures.notebook
        sig_idx = notebook.get_current_page()
        sig: SignaturePage = notebook.get_nth_page(sig_idx)
        notebook = sig.attributes.notebook

        idx = notebook.get_current_page()
        if idx > -1:
            # save others attributes before to delete the current
            model_sig = self.model.current.signatures[sig_idx]
            for i in range(notebook.get_n_pages()):
                attr = self._load_attribute_from_page(notebook.get_nth_page(i))
                model_sig.attributes[i] = attr
            widget = notebook.get_nth_page(idx)
            notebook.remove_page(idx)
            widget.destroy()
            del model_sig.attributes[idx]
            self._refresh_item_class_signatures()

    def on_added_child(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        notebook = self.view.info.signatures.notebook
        sig_idx = notebook.get_current_page()
        sig: SignaturePage = notebook.get_nth_page(sig_idx)
        notebook = sig.children.notebook

        # save others attributes before to add a new one
        model_sig: SignatureModel = self.model.current.signatures[sig_idx]
        for i in range(notebook.get_n_pages()):
            page = notebook.get_nth_page(i)
            model_sig.children[i] = self._load_child_from_page(page)

        model_sig.children.append(ChildModel(''))
        self._refresh_item_class_signatures()
        notebook.set_current_page(len(model_sig.attributes))

    def on_removed_child(self, clicked: Gtk.Button):
        if self.view.menu.view_env.get_active() or self.model.current is None:
            return
        notebook = self.view.info.signatures.notebook
        sig_idx = notebook.get_current_page()
        sig: SignaturePage = notebook.get_nth_page(sig_idx)
        notebook = sig.children.notebook
        idx = notebook.get_current_page()
        if idx > -1:
            # save others attributes before to delete the current
            model_sig = self.model.current.signatures[sig_idx]
            for i in range(notebook.get_n_pages()):
                attr = self._load_child_from_page(notebook.get_nth_page(i))
                model_sig.children[i] = attr
            widget = notebook.get_nth_page(idx)
            notebook.remove_page(idx)
            widget.destroy()
            del model_sig.children[idx]
            self._refresh_item_class_signatures()
