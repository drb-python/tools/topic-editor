import os
import tempfile
import graphviz

from .manager import ModelManager


def display_graph(model: ModelManager):
    current = model.current
    if current is None:
        return
    fd, path = tempfile.mkstemp(suffix='.gv')
    graph = graphviz.Digraph(current.label, filename=path)
    graph.attr(rankdir='RL', size='8.5')
    graph.node('NO', label=current.label)

    parent = current.subclass_of
    if parent is not None:
        parent = model.get_itemclass(parent)
        graph.node('NP', label=parent.label)
        graph.edge('NO', 'NP')

    classes = model.loaded + model.working
    for eic in classes:
        if eic.subclass_of == current.id:
            graph.edge(eic.label, 'NO')

    graph.view()
    # close and delete temporary files
    os.close(fd)
    os.remove(path)
