from __future__ import annotations
import uuid
import random
import string

import yaml
from typing import List
from drb.factory import ItemClass, ItemClassType


def random_string() -> str:
    return 'topic#' + \
        ''.join(random.choice(string.digits) for _ in range(5))


class AttributeModel:
    def __init__(self, name: str, namespace: str = '', value: str = ''):
        if name is None:
            raise ValueError
        self.name = name
        self.namespace = namespace
        self.value = value

    def to_dict(self) -> dict:
        data = {'name': self.name}
        if self.namespace is not None and self.namespace != '':
            data['namespace'] = self.namespace
        if self.value is not None and self.value != '':
            data['value'] = eval(self.value)
        return data


class ChildModel:
    def __init__(self, name: str, namespace: str = '', aware: bool = False):
        if name is None:
            raise ValueError
        self.name = name
        self.namespace = namespace
        self.namespace_aware = aware

    def to_dict(self) -> dict:
        data = {'name': self.name}
        if self.namespace is not None and self.namespace != '':
            data['namespace'] = self.namespace
        if self.namespace_aware:
            data['namespaceAware'] = True
        return data


class SignatureModel:
    def __init__(self, **kwargs):
        self.name = kwargs.get('name', None)
        self.namespace = kwargs.get('namespace', None)
        self.path = kwargs.get('path', None)
        self.attributes = []
        if 'attributes' in kwargs:
            for attr in kwargs['attributes']:
                self.attributes.append(AttributeModel(
                    attr['name'],
                    attr.get('namespace', ''),
                    attr.get('value', '')))
        self.children = []
        if 'children' in kwargs:
            for child in kwargs['children']:
                self.children.append(ChildModel(
                    child['name'],
                    child.get('namespace', ''),
                    child.get('namespaceAware', False)))
        self.xquery = kwargs.get('xquery', None)
        self.python = kwargs.get('python', None)

    def to_dict(self) -> dict:
        data = {}
        if self.name is not None and self.name != '':
            data['name'] = self.name
        if self.namespace is not None and self.namespace != '':
            data['namespace'] = self.namespace
        if self.path is not None and self.path != '':
            data['path'] = self.path
        if len(self.attributes) > 0:
            attributes = []
            for attr in self.attributes:
                attributes.append(attr.to_dict())
            data['attributes'] = attributes
        if len(self.children) > 0:
            children = []
            for child in self.children:
                children.append(child.to_dict())
            data['children'] = children
        if self.xquery is not None and self.xquery != '':
            data['xquery'] = self.xquery
        if self.python is not None and self.python != '':
            data['python'] = self.python
        return data


class ItemClassModel:
    _ITEM_CLASS_TYPE_TAB = [x for x in ItemClassType]

    @classmethod
    def itemclass_types(cls):
        return cls._ITEM_CLASS_TYPE_TAB.copy()

    @classmethod
    def from_yaml_data(cls, path: str) -> List[ItemClassModel]:
        try:
            item_classes = []
            with open(path, 'r') as f:
                documents = yaml.safe_load_all(f)
                for doc in documents:
                    item_classes.append(ItemClassModel(**doc))
            return item_classes
        except Exception as ex:
            print(ex)
            return []

    @classmethod
    def from_item_class(cls, ic: ItemClass) -> ItemClassModel:
        eic = ItemClassModel()
        eic.id = str(ic.id)
        eic.label = ic.label
        eic.subclass_of = str(ic.parent_class_id) \
            if ic.parent_class_id is not None else None
        eic.description = ic.description if ic.description is not None else ''
        eic.category = cls._ITEM_CLASS_TYPE_TAB.index(ic.category)
        eic.signatures = [SignatureModel(**s.to_dict()) for s in ic.signatures]
        return eic

    def __init__(self, **kwargs):
        self.id = kwargs.get('id', str(uuid.uuid4()))
        self.label = kwargs.get('label', random_string())
        self.subclass_of = kwargs.get('subClassOf', None)
        self.override = False
        self.description = kwargs.get('description', '')
        category = ItemClassType.CONTAINER
        if 'category' in kwargs:
            category = ItemClassType(kwargs['category'])
        self.category = self._ITEM_CLASS_TYPE_TAB.index(category)
        self.factory = None
        self.signatures = []
        if 'signatures' in kwargs:
            for sig in kwargs['signatures']:
                self.signatures.append(SignatureModel(**sig))

    def to_dict(self) -> dict:
        data = {
            'id': self.id,
            'label': self.label,
            'category': self._ITEM_CLASS_TYPE_TAB[self.category].value,
            'signatures': [sig.to_dict() for sig in self.signatures]
        }
        if self.subclass_of is not None:
            data['subClassOf'] = self.subclass_of
        if self.description is not None and self.description != '':
            data['description'] = self.description
        return data

    def __repr__(self):
        return f'EditableItemClass[{self.label}]'

    def __eq__(self, other):
        if isinstance(other, ItemClassModel):
            return self.id == other.id
        return False
