from typing import List

import yaml
from drb.factory import ItemClassLoader, ItemClassType

from .model import ItemClassModel


def repr_str(dumper: yaml.SafeDumper, data):
    if '\n' in data:
        return dumper.represent_scalar(
            u'tag:yaml.org,2002:str', data, style='|')
    return dumper.org_represent_str(data)


yaml.SafeDumper.org_represent_str = yaml.SafeDumper.represent_str
yaml.add_representer(str, repr_str, Dumper=yaml.SafeDumper)


class ModelManager:
    def __init__(self):
        self._current: ItemClassModel = None
        self._possible_subclasses = None
        self.working: List[ItemClassModel] = []
        self.loaded: List[ItemClassModel] = []
        for ic in ItemClassLoader().get_all_item_classes():
            self.loaded.append(ItemClassModel.from_item_class(ic))

    @property
    def current(self) -> ItemClassModel:
        return self._current

    @current.setter
    def current(self, eic: ItemClassModel):
        self._possible_subclasses = [None]
        if eic is None:
            self._current = None
            subclasses = self.loaded + self.working
        else:
            self._current = eic
            subclasses = self._get_current_possible_subclass()
        self._possible_subclasses.extend(subclasses)

    @property
    def current_possible_subclasses(self) -> List[ItemClassModel]:
        return self._possible_subclasses

    def get_highest_loaded_itemclasses(self) -> List[ItemClassModel]:
        return [x for x in self.loaded if x.subclass_of is None]

    def get_highest_working_itemclass(self) -> List[ItemClassModel]:
        return [x for x in self.working if x.subclass_of is None]

    def get_itemclass_types(self) -> List[ItemClassType]:
        return ItemClassModel.itemclass_types()

    def get_itemclass_type_index(self, itemclass_type: ItemClassType) -> int:
        return self.get_itemclass_types().index(itemclass_type)

    def get_itemclass(self, identifier) -> ItemClassModel:
        try:
            eic = next(filter(lambda x: x.id == identifier, self.loaded))
        except StopIteration:
            eic = next(filter(lambda x: x.id == identifier, self.working))
        return eic

    def get_current_subclasses(self, eic: ItemClassModel = None,
                               classes: List[ItemClassModel] = None) -> \
            List[ItemClassModel]:
        result = []
        if eic is None:
            eic = self.current
        if classes is None:
            classes = self.loaded + self.working
        subclasses = [cls for cls in classes if cls.subclass_of == eic.id]
        for cls in subclasses:
            result.append(cls)
            result.extend(self.get_current_subclasses(cls, classes))
        return result

    def _get_current_possible_subclass(self) -> List[ItemClassModel]:
        temp = self.loaded + self.working
        subclasses = self.get_current_subclasses()
        return [eic for eic in temp
                if eic not in subclasses and eic != self.current]

    def load(self, path: str):
        self.working = ItemClassModel.from_yaml_data(path)

    def dump(self, stream=None):
        documents = [doc.to_dict() for doc in self.working]
        if stream is None:
            return yaml.safe_dump_all(documents, sort_keys=False)
        yaml.safe_dump_all(documents, stream, sort_keys=False)
