import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio
from .model import ModelManager
from .controller import ItemClassController, GraphController


class ItemClassEditorApp(Gtk.Application):
    WINDOW_WIDTH_PX = 1024
    WINDOW_HEIGHT_PX = 768

    def __init__(self, app_id: str) -> None:
        super().__init__(application_id=app_id,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.model = ModelManager()
        self.item_class_ctrl = ItemClassController(self.model)
        self.graph_ctrl = None

    def do_activate(self) -> None:
        self.item_class_ctrl.view.set_application(self)
        self.item_class_ctrl.initialize()
        self.item_class_ctrl.view.show_all()

    def do_window_removed(self, window: Gtk.Window):
        if window == self.item_class_ctrl.view:
            self.quit()
        self.graph_ctrl = None
        self.item_class_ctrl.view.show_all()

    def switch_to_graph_view(self):
        if self.model.current is None:
            print('No item class selected')
            return
        if self.graph_ctrl is None:
            self.graph_ctrl = GraphController(self.model)
            self.graph_ctrl.view.set_application(self)
            self.graph_ctrl.initialize()
            self.item_class_ctrl.view.set_visible(False)
            self.graph_ctrl.view.show_all()

    def get_default_width(self) -> int:
        return self.WINDOW_WIDTH_PX

    def get_default_height(self) -> int:
        return self.WINDOW_HEIGHT_PX
