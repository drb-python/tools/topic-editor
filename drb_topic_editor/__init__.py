from .app import ItemClassEditorApp
from . import _version


__version__ = _version.get_versions()['version']
__all__ = ['ItemClassEditorApp']


def main():
    ItemClassEditorApp(None).run()
