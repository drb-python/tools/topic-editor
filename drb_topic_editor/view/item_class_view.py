from gi.repository import Gtk
from .common import DefaultFrame, FrameEntry, FrameComboboxText, \
    FrameTextView, FrameNoteBook


class AttributePage(Gtk.Box):
    def __init__(self):
        super().__init__()
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.set_spacing(5)
        self.name = FrameEntry('Name')
        self.namespace = FrameEntry('Namespace')
        self.value = FrameEntry('Value')
        self.add(self.name)
        self.add(self.namespace)
        self.add(self.value)


class ChildPage(Gtk.Box):
    def __init__(self):
        super().__init__()
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.set_spacing(5)
        self.name = FrameEntry('Name')
        self.namespace = FrameEntry('Namespace')
        self.ns_aware = Gtk.CheckButton()
        self.add(self.name)
        self.add(self.namespace)
        self.add(self.ns_aware)


class SignaturePage(Gtk.Box):
    def __init__(self):
        super().__init__()
        self.name = FrameEntry('Name')
        self.namespace = FrameEntry('Namespace')
        self.path = FrameEntry('Path')
        self.attributes = FrameNoteBook('Attributes', page_prefix='attr')
        self.children = FrameNoteBook('Children', page_prefix='child')
        self.xquery = FrameTextView('Xquery')
        self.python = FrameTextView('Python')

        signal = 'toggled'
        self.button_name = Gtk.CheckButton(label='Name')
        self.button_name.connect(signal, self.on_toggled_button, self.name)
        self.button_namespace = Gtk.CheckButton(label='Namespace')
        self.button_namespace.connect(
            signal, self.on_toggled_button, self.namespace)
        self.button_path = Gtk.CheckButton(label='Path')
        self.button_path.connect(signal, self.on_toggled_button, self.path)
        self.button_attributes = Gtk.CheckButton(label='Attributes')
        self.button_attributes.connect(
            signal, self.on_toggled_button, self.attributes)
        self.button_children = Gtk.CheckButton(label='Children')
        self.button_children.connect(
            signal, self.on_toggled_button, self.children)
        self.button_xquery = Gtk.CheckButton(label='XQuery')
        self.button_xquery.connect(
            signal, self.on_toggled_button, self.xquery)
        self.button_python = Gtk.CheckButton(label='Python')
        self.button_python.connect(
            signal, self.on_toggled_button, self.python)

        button_box = Gtk.Box(spacing=5)
        button_box.set_orientation(Gtk.Orientation.HORIZONTAL)
        button_box.set_homogeneous(True)
        button_box.pack_start(self.button_name, False, False, 0)
        button_box.pack_start(self.button_namespace, False, False, 0)
        button_box.pack_start(self.button_path, False, False, 0)
        button_box.pack_start(self.button_attributes, False, False, 0)
        button_box.pack_start(self.button_children, False, False, 0)
        button_box.pack_start(self.button_xquery, False, False, 0)
        button_box.pack_start(self.button_python, False, False, 0)

        self.set_spacing(5)
        self.set_orientation(Gtk.Orientation.VERTICAL)
        self.pack_start(button_box, False, True, 5)
        self.pack_start(self.name, False, True, 5)
        self.pack_start(self.namespace, False, True, 5)
        self.pack_start(self.path, False, True, 5)
        self.pack_start(self.attributes, False, True, 5)
        self.pack_start(self.children, False, True, 5)
        self.pack_start(self.xquery, False, True, 5)
        self.pack_start(self.python, False, True, 5)

    def initialize(self):
        self.name.set_visible(False)
        self.namespace.set_visible(False)
        self.path.set_visible(False)
        self.attributes.set_visible(False)
        self.children.set_visible(False)
        self.xquery.set_visible(False)
        self.python.set_visible(False)

    def _at_least_one_button_activated(self):
        return any([
            self.button_name.get_active(),
            self.button_namespace.get_active(),
            self.button_path.get_active(),
            self.button_attributes.get_active(),
            self.button_children.get_active(),
            self.button_xquery.get_active(),
            self.button_python.get_active()
        ])

    def on_toggled_button(self, button: Gtk.CheckButton, widget: Gtk.Widget):
        activated = button.get_active()
        widget.set_visible(activated)
        if not activated and not self._at_least_one_button_activated():
            button.set_active(True)


class ItemClassEditorMenu(Gtk.MenuBar):
    def __init__(self):
        super().__init__()
        self._file_menu = Gtk.Menu()
        self.file_new = Gtk.MenuItem(label='New')
        self.file_save = Gtk.MenuItem(label='Save')
        self.file_load = Gtk.MenuItem(label='Load')
        self.file_exit = Gtk.MenuItem(label='Exit')
        self._file_menu.append(self.file_new)
        self._file_menu.append(self.file_save)
        self._file_menu.append(self.file_load)
        self._file_menu.append(self.file_exit)
        file_menu_item = Gtk.MenuItem(label='File')
        file_menu_item.set_submenu(self._file_menu)

        view_menu_item = Gtk.MenuItem(label='View')
        self._view_menu = Gtk.Menu()
        self.view_env = Gtk.CheckMenuItem(label='Environment')
        self._view_menu.append(self.view_env)
        view_menu_item.set_submenu(self._view_menu)

        self.view_graph = Gtk.MenuItem(label='Graph')

        self.append(file_menu_item)
        self.append(view_menu_item)
        self.append(self.view_graph)


class ItemClassTreeView(Gtk.TreeView):
    def __init__(self):
        super().__init__()


class ItemClassInfoView(DefaultFrame):
    def __init__(self):
        super().__init__('Item Class')
        self.id = FrameEntry('Id')
        self.label = FrameEntry('Label')
        self.subclass_of = FrameComboboxText('Subclass Of')
        self.category = FrameComboboxText('Category')
        self.description = FrameTextView('Description')
        self.factory = FrameComboboxText('Factory')
        self.signatures = FrameNoteBook('Signatures')

        box = Gtk.Box()
        box.set_orientation(Gtk.Orientation.VERTICAL)
        box.set_spacing(5)
        box.set_margin_top(5)
        box.set_margin_bottom(5)
        box.set_margin_left(5)
        box.set_margin_right(5)
        box.pack_start(self.id, False, True, 5)
        box.pack_start(self.label, False, True, 5)
        box.pack_start(self.subclass_of, False, True, 5)
        box.pack_start(self.category, False, True, 5)
        box.pack_start(self.description, False, True, 5)
        box.pack_start(self.factory, False, True, 5)
        box.pack_start(self.signatures, False, True, 5)
        self.add(box)


class ItemClassWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self._menu = ItemClassEditorMenu()
        self._tree = ItemClassTreeView()
        self._info = ItemClassInfoView()

        self._scroll_tree = Gtk.ScrolledWindow()
        self._scroll_tree.add(self._tree)

        self._scroll_info = Gtk.ScrolledWindow()
        self._scroll_info.add(self._info)

        self._tree_info_box = Gtk.Box()
        self._tree_info_box.set_orientation(Gtk.Orientation.HORIZONTAL)
        self._tree_info_box.set_spacing(0)
        self._tree_info_box.pack_start(self._scroll_tree, False, False, 0)
        self._tree_info_box.pack_start(self._scroll_info, True, True, 0)

        self._main_box = Gtk.Box()
        self._main_box.set_orientation(Gtk.Orientation.VERTICAL)
        self._main_box.set_spacing(0)
        self._main_box.pack_start(self._menu, False, False, 0)
        self._main_box.pack_start(self._tree_info_box, True, True, 0)
        self.add(self._main_box)
        self.connect('check-resize', self.on_check_resize)

    @property
    def menu(self) -> ItemClassEditorMenu:
        return self._menu

    @property
    def tree(self) -> ItemClassTreeView:
        return self._tree

    @property
    def info(self) -> ItemClassInfoView:
        return self._info

    def on_check_resize(self, container: Gtk.Container):
        width = container.get_allocated_width()
        width -= container.get_margin_left() + container.get_margin_right()
        height = self._scroll_tree.get_allocated_height()
        self._scroll_tree.set_size_request(width // 4, height)
