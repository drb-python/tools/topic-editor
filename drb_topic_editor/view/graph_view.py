from gi.repository import Gtk
from .common import DefaultFrame, FrameComboboxText


class GraphMenu(Gtk.MenuBar):
    def __init__(self):
        super().__init__()
        file_menu = Gtk.Menu()
        self.file_save = Gtk.MenuItem(label='Save')
        file_menu.append(self.file_save)
        file_menu_item = Gtk.MenuItem(label='File')
        file_menu_item.set_submenu(file_menu)
        self.append(file_menu_item)


class FrameSpinRangeButton(DefaultFrame):
    def __init__(self, label: str):
        super().__init__(label)
        self.button = Gtk.SpinButton()
        self.button.set_margin_top(3)
        self.button.set_margin_bottom(3)
        self.button.set_margin_left(3)
        self.button.set_margin_right(3)
        self.add(self.button)


class GraphParamView(DefaultFrame):
    def __init__(self):
        super().__init__('Parameters')
        self.engine = FrameComboboxText('Engine')
        self.orientation = FrameComboboxText('Orientation')
        self.size = FrameSpinRangeButton('Size')
        self.unflatten = None

        box = Gtk.Box()
        box.set_orientation(Gtk.Orientation.VERTICAL)
        box.set_spacing(5)
        box.pack_start(self.engine, False, True, 5)
        box.pack_start(self.orientation, False, True, 5)
        box.pack_start(self.size, False, True, 5)

        self.add(box)


class GraphWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.menu = GraphMenu()
        self.params = GraphParamView()
        self.graph = Gtk.Image()

        scroll = Gtk.ScrolledWindow()
        scroll.add(self.graph)

        main_box = Gtk.Box()
        main_box.set_spacing(3)
        main_box.set_orientation(Gtk.Orientation.HORIZONTAL)
        main_box.set_margin_top(5)
        main_box.set_margin_bottom(5)
        main_box.set_margin_left(5)
        main_box.set_margin_right(5)
        main_box.pack_start(self.params, False, False, 5)
        main_box.pack_start(scroll, True, True, 5)

        win_box = Gtk.Box()
        win_box.set_spacing(0)
        win_box.set_orientation(Gtk.Orientation.VERTICAL)
        win_box.pack_start(self.menu, False, False, 0)
        win_box.pack_start(main_box, True, True, 0)

        self.add(win_box)
        self.connect('check-resize', self.on_check_resize)

    def on_check_resize(self, container: Gtk.Container):
        width = container.get_allocated_width()
        width -= container.get_margin_left() + container.get_margin_right()
        height = self.params.get_allocated_height()
        self.params.set_size_request(width // 4, height)
