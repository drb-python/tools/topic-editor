from gi.repository import Gtk


class AddDelButtons(Gtk.HBox):
    def __init__(self):
        super().__init__()
        self.add_button = Gtk.Button(label='+')
        self.del_button = Gtk.Button(label='-')
        self.pack_start(self.add_button, False, False, 10)
        self.pack_start(self.del_button, False, False, 10)
        self.set_homogeneous(True)


class DefaultFrame(Gtk.Frame):
    def __init__(self, label: str):
        super().__init__()
        self.set_label(label)
        self.set_margin_left(10)
        self.set_margin_right(10)
        self.set_label_align(0.03, 0.5)


class FrameEntry(DefaultFrame):
    def __init__(self, label: str):
        super().__init__(label)
        self.entry = Gtk.Entry()
        self.entry.set_margin_left(3)
        self.entry.set_margin_right(3)
        self.entry.set_margin_top(3)
        self.entry.set_margin_bottom(3)
        self.add(self.entry)


class FrameComboboxText(DefaultFrame):
    def __init__(self, label: str):
        super().__init__(label)
        self.combobox = Gtk.ComboBoxText()
        self.combobox.set_margin_top(3)
        self.combobox.set_margin_bottom(3)
        self.combobox.set_margin_left(3)
        self.combobox.set_margin_right(3)
        self.add(self.combobox)


class FrameTextView(DefaultFrame):
    def __init__(self, label: str):
        super().__init__(label)
        self.text = Gtk.TextView()
        self.text.set_margin_top(3)
        self.text.set_margin_bottom(3)
        self.text.set_margin_left(3)
        self.text.set_margin_right(3)
        self.add(self.text)


class FrameNoteBook(DefaultFrame):
    def __init__(self, label: str, **kwargs):
        super().__init__(label)
        self._page_prefix = kwargs.get('page_prefix', '')
        self.buttons = AddDelButtons()
        self.notebook = Gtk.Notebook()
        box = Gtk.Box(spacing=5)
        box.set_orientation(Gtk.Orientation.VERTICAL)
        box.set_margin_top(3)
        box.set_margin_bottom(3)
        box.set_margin_left(3)
        box.set_margin_right(3)
        box.pack_start(self.buttons, False, True, 0)
        box.pack_start(self.notebook, False, True, 0)
        self.add(box)

    def set_page_prefix(self, prefix: str):
        self._page_prefix = prefix if prefix is not None else ''

    def clear(self):
        for idx in range(self.notebook.get_n_pages()):
            self.notebook.remove_page(0)

    def append_page(self, page):
        idx = self.notebook.get_n_pages()
        self.notebook.append_page(
            page, Gtk.Label(label=f'{self._page_prefix}#{idx}'))
        self.notebook.set_current_page(idx)
        self.notebook.get_nth_page(idx).show_all()

    def remove_page(self):
        idx = self.notebook.get_current_page()
        if idx >= 0:
            self.notebook.remove_page(idx)
            for p_idx in range(idx, self.notebook.get_n_pages()):
                child = self.notebook.get_nth_page(p_idx)
                self.notebook.set_tab_label_text(
                    child, f'{self._page_prefix}#{p_idx}')
        self.notebook.show_all()


class FileChooser(Gtk.FileChooserDialog):
    def __init__(self, action: Gtk.FileChooserAction):
        super().__init__('Please choose a file', action=action)
        button_label = 'Open'
        if action == Gtk.FileChooserAction.SAVE:
            button_label = 'Save'
        self.add_button(button_label, Gtk.ResponseType.OK)
        self.add_button('Cancel', Gtk.ResponseType.CANCEL)
        self.set_default_response(Gtk.ResponseType.CANCEL)
