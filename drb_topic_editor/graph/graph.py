import os
import tempfile
import graphviz

from ..model import ModelManager


class GraphManager:
    default_format = 'png'
    default_size = 8

    def __init__(self, model: ModelManager):
        self._model = model
        fd, path = tempfile.mkstemp(suffix='.gv')
        os.close(fd)
        self._temp_graph_file = path
        self._temp_graph_rendered = f'{path}.{self.default_format}'
        self._graph = graphviz.Digraph(self._temp_graph_file,
                                       format=self.default_format)

        current = model.current
        self._graph.node('NO', label=model.current.label)
        parent = current.subclass_of
        if parent is not None:
            parent = model.get_itemclass(parent)
            self._graph.node('NP', label=parent.label)
            self._graph.edge('NO', 'NP')
        classes = model.loaded + model.working
        for eic in classes:
            if eic.subclass_of == current.id:
                self._graph.edge(eic.label, 'NO')

    def generate(self, **kwargs) -> str:
        engine = kwargs.get('engine', 'dot')
        orientation = kwargs.get('orientation', 'TB')
        size = kwargs.get('size', self.default_size)
        unflatten = kwargs.get('size', -1)

        self._graph.attr('graph', engine=engine)
        self._graph.attr(rankdir=orientation)
        if size > 0:
            self._graph.attr(size=str(size))
        if unflatten > 0:
            self._graph.unflatten(stagger=unflatten)

        self._graph.render(outfile=self._temp_graph_rendered,
                           engine=engine,
                           format=self.default_format)
        return self._temp_graph_rendered

    def save_graph_to(self, dest: str):
        extension = f'.{self.default_format}'
        if dest.endswith(extension):
            path = dest
        else:
            path = f'{dest}{extension}'
        self._graph.render(outfile=path,
                           format=self.default_format)
        path = path[:-len(extension)] + '.gv'
        if os.path.exists(path):
            os.remove(path)

    def close(self):
        del self._graph
        os.remove(self._temp_graph_file)
        if os.path.exists(self._temp_graph_rendered):
            os.remove(self._temp_graph_rendered)
